//
//  AlarmViewController.m
//  Alarm
//
//  Created by Masaki Hirokawa on 2013/09/04.
//  Copyright (c) 2013 Masaki Hirokawa. All rights reserved.
//

#import "AlarmViewController.h"

@interface AlarmViewController ()

@property UIButton      *stopAlarmButton;  //アラーム停止ボタン
@property AVAudioPlayer *alarmPlayer;      //アラームプレイヤー
@property UIDatePicker  *alarmPicker;      //起床時間の選択ピッカー
@property NSTimer       *alarmTimer;       //アラームタイマー
@property BOOL          isStartedAlarm;    //アラーム開始フラグ
@property UIView *personButtonContainer;   //開始ボタンのコンテナ
@property (weak, nonatomic) IBOutlet UIImageView *alarmOnIcon;
@end

@implementation AlarmViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //アラーム開始ボタン配置
    [self setAlarmControlButton];
    
    //起床時間の選択ピッカー配置
    [self setWakeUpTimePicker];
}

//debug
- (IBAction)onTapHiddenButton:(id)sender {
    [self clearAlarmTimer];
    [self playAlarm];
}

#pragma mark alarm control button

//アラームコントロールボタン配置
- (void)setAlarmControlButton
{
    self.personButtonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    [self.view addSubview:self.personButtonContainer];
    
    int buttonWidth = 75;
    int margin = 4;
    for (int i=0; i<8; i++) {
        UIButton *alarmControlButton = [DCButton imageButton:CGRectMake((buttonWidth+margin) * (i%4) + margin, (buttonWidth+margin) * (i/4),
                                                                        buttonWidth, buttonWidth)
                                                         img:[UIImage imageNamed:THEMES[i]]
                                                isHighlighte:true
                                                      on_img:[UIImage imageNamed:THEMES[i]]
                                                    delegate:self
                                                      action:@selector(onTapStartAlarmButton:)
                                                         tag:i];

        [self.personButtonContainer addSubview:alarmControlButton];
    }
    
    UIButton *stopButton = [DCButton imageButton:STOP_BUTTON_RECT
                                                     img:[UIImage imageNamed:@"blank"]
                                            isHighlighte:false
                                                  on_img:[UIImage imageNamed:@"blank"]
                                                delegate:self
                                                  action:@selector(onTapStopAlarmButton:)
                                                     tag:9999];
    
    [self.view addSubview:stopButton];
    self.stopAlarmButton = stopButton;
    self.stopAlarmButton.hidden = YES;
    self.alarmOnIcon.hidden = YES;
}

//アラーム開始
- (void)onTapStartAlarmButton:(UIButton *)button
{
    if(_isStartedAlarm){
        [self onTapStopAlarmButton:button];
        return;
    }
    
    NSUInteger themeNum = button.tag;
    
    //現在時刻と設定時刻が同じならアラートを表示し処理しない
    if ([self isCurrentTime]) {
        [DCUtil showAlert:nil message:ALARM_ERROR_MESSAGE
        cancelButtonTitle:nil otherButtonTitles:@"OK"];
        return;
    }
    
    //アラームタイマー開始
    [self startAlarmTimer];
    _isStartedAlarm = true;
    
    self.alarmOnIcon.hidden = NO;
        
    _alarmPlayer = [[DCAudioPlayer alloc] initWithAudio:THEMES[themeNum] ext:ALARM_FILE_EXT isUseDelegate:NO];
    
    _stopAlarmButton.imageView.image = [UIImage imageNamed:THEMES[themeNum]];
}

//アラーム停止
- (void)onTapStopAlarmButton:(UIButton *)button
{
    //アラームタイマー停止
    [self clearAlarmTimer];
    
    //アラーム停止
    [self stopAlarm];
    _isStartedAlarm = false;
    self.alarmOnIcon.hidden = YES;
}

#pragma mark alarm time

//アラームタイマー開始
- (void)startAlarmTimer
{
    if (_isStartedAlarm) {
        return;
    }
    
    //スリープ禁止
    [APP_DELEGATE setIdleTimer:YES];
    
    //アラート表示
    [DCUtil showAlert:nil message:ALARM_START_MESSAGE
    cancelButtonTitle:nil otherButtonTitles:@"OK"];
    
    //タイマー停止
    [self clearAlarmTimer];
    
    //タイマー開始
    _alarmTimer = [NSTimer scheduledTimerWithTimeInterval:ALARM_TIMER_INTERVAL target:self
                                                 selector:@selector(alarmTimerEvent:) userInfo:nil
                                                  repeats:YES];
}

//アラームタイマーイベント
- (void)alarmTimerEvent:(NSTimer *)timer
{
    //現在時刻が設定時刻であればアラームを鳴らす
    if ([self isCurrentTime]) {
        if (_isStartedAlarm) {
            //タイマー停止
            [self clearAlarmTimer];
            
            //アラーム再生
            [self playAlarm];
        }
    }
}

//アラームタイマー停止
- (void)clearAlarmTimer
{
    if (!_isStartedAlarm) {
        return;
    }
    
    //スリープ許可
    [APP_DELEGATE setIdleTimer:NO];
    
    //タイマー停止
    if (_alarmTimer != NULL) {
        [_alarmTimer invalidate];
    }
    self.personButtonContainer.hidden = YES;
}

#pragma mark play/stop alarm

//指定したアラームの再生
- (void)playAlarm
{
    [_alarmPlayer setNumberOfLoops:ALARM_PLAY_INFINITE];
    [_alarmPlayer play];
    
    self.personButtonContainer.hidden = YES;
    self.stopAlarmButton.hidden = NO;
}

//指定したアラームの停止
- (void)stopAlarm
{
    if ([_alarmPlayer isPlaying]) [_alarmPlayer stop];
    //アラート表示
    [DCUtil showAlert:nil message:ALARM_STOP_MESSAGE
    cancelButtonTitle:nil otherButtonTitles:@"OK"];
    
    self.personButtonContainer.hidden = NO;
    self.stopAlarmButton.hidden = YES;
}

#pragma mark date picker

//起床時間の選択ピッカー配置
- (void)setWakeUpTimePicker
{
    //起床時間の選択ピッカーを入れるビュー追加
    UIView *wakeUpTimePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height- ALARM_PICKER_HEIGHT, ALARM_PICKER_WIDTH, ALARM_PICKER_HEIGHT)];
    [self.view addSubview:wakeUpTimePickerView];
    
    //起床時間の選択ピッカー初期化
    _alarmPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, ALARM_PICKER_WIDTH, ALARM_PICKER_HEIGHT)];
    
    //日付の表示モードを変更する(時分を表示)
    _alarmPicker.datePickerMode = UIDatePickerModeTime;
    
    //何分刻みにするか
    _alarmPicker.minuteInterval = ALARM_PICKER_MINUTE_INTERVAL;
    
    //初期時刻設定
    [_alarmPicker setDate:[self wakeUpDate]];
    
    //起床時間の選択ピッカーの値が変更されたときに呼ばれるメソッドを設定
    [_alarmPicker addTarget:self
                     action:@selector(alarmPickerChanged:)
           forControlEvents:UIControlEventValueChanged];
    
    //UIDatePickerのインスタンスをビューに追加
    [wakeUpTimePickerView addSubview:_alarmPicker];
}

//起床時間の選択ピッカー変更時のイベント
- (void)alarmPickerChanged:(UIDatePicker *)datePicker
{
    //アラーム開始フラグを下ろす
    _isStartedAlarm = NO;
    self.alarmOnIcon.hidden = YES;
    
    //タイマー停止
    [self clearAlarmTimer];
}

#pragma mark getter method

//起床時間のテキスト取得
- (NSString *)wakeUpTimeText
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = ALARM_TIME_DATE_FORMAT;
    return [dateFormatter stringFromDate:_alarmPicker.date];
}

//起床時間取得
- (NSDate *)wakeUpDate
{
    NSString *wakeUpDateString = wakeUpTime;
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:ALARM_TIME_DATE_FORMAT];
    NSDate *wakeUpDate = [dateFormater dateFromString:wakeUpDateString];
    return wakeUpDate;
}

//UIDatePickerで指定されている時刻(時)取得
- (NSInteger)wakeUpDatePickerHour
{
    NSDateFormatter *hourFormatter = [[NSDateFormatter alloc] init];
    [hourFormatter setLocale:[NSLocale currentLocale]];
    [hourFormatter setDateFormat:@"HH"];
    NSString *datePickerHour = [hourFormatter stringFromDate:_alarmPicker.date];
    return [datePickerHour intValue];
}

//UIDatePickerで指定されている時刻(分)取得
- (NSInteger)wakeUpDatePickerMinute
{
    NSDateFormatter *minuteFormatter = [[NSDateFormatter alloc] init];
    [minuteFormatter setLocale:[NSLocale currentLocale]];
    [minuteFormatter setDateFormat:@"mm"];
    NSString *datePickerMinute = [minuteFormatter stringFromDate:_alarmPicker.date];
    return [datePickerMinute intValue];
}

//現在時刻であるか
- (BOOL)isCurrentTime
{
    return ([self currentHour] == [self wakeUpDatePickerHour] &&
            [self currentMinute] == [self wakeUpDatePickerMinute]);
}
//現在の日付を取得
- (NSInteger)currentDay
{
    NSDateComponents *currentTimeComponents = [self currentDateComponents];
    return currentTimeComponents.day;
}

//現在の時間を取得
- (NSInteger)currentHour
{
    NSDateComponents *currentTimeComponents = [self currentDateComponents];
    return currentTimeComponents.hour;
}

//現在の分数を取得
- (NSInteger)currentMinute
{
    NSDateComponents *currentTimeComponents = [self currentDateComponents];
    return currentTimeComponents.minute;
}

//現在時刻のコンポーネント取得
- (NSDateComponents *)currentDateComponents
{
    //現在の時刻を取得
    NSDate *nowDate = [NSDate date];
    
    //現在時刻のコンポーネント定義
    NSDateComponents *nowComponents;
    nowComponents = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit |
                                                              NSMinuteCalendarUnit |
                                                              NSSecondCalendarUnit)
                                                    fromDate:nowDate];
    return nowComponents;
}

@end
