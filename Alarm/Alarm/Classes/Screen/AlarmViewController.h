//
//  AlarmViewController.h
//  Alarm
//
//  Created by Masaki Hirokawa on 2013/09/04.
//  Copyright (c) 2013 Masaki Hirokawa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCAudioPlayer.h"
#import "DCLabel.h"
#import "DCButton.h"
#import "DCUtil.h"

#define ALARM_FILE_EXT               @"m4a"
#define ALARM_PICKER_WIDTH           320
#define ALARM_PICKER_HEIGHT          192
#define ALARM_PICKER_MINUTE_INTERVAL 1
#define ALARM_PLAY_INFINITE          -1
#define ALARM_TIME_DATE_FORMAT       @"HH:mm"
#define ALARM_TIMER_INTERVAL         1
#define ALARM_START_MESSAGE          @"アラームを開始しました。アプリを終了したりスリープさせないでください。"
#define ALARM_STOP_MESSAGE          @"アラームを停止しました。"
#define ALARM_ERROR_MESSAGE          @"指定時刻と現在時刻が同じです。"
#define THEMES @[@"gri_yuya", @"chaos_sano", @"gang2_bun", @"hunter_uni", @"yoshida", @"mono_tanacyu", @"lom_kamon", @"shogo"]
#define STOP_BUTTON_RECT CGRectMake(70, 150, 180, 180)

@interface AlarmViewController : UIViewController
@end
